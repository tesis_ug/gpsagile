package com.claro.ec.agilesanchezmicroservice.infraestructura;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.claro.ec.agilesanchezmicroservice.documentos.Track;

import java.util.concurrent.CompletableFuture;

@CrossOrigin(origins = { "*" }, maxAge = 3000)

@RequestMapping("/agilasanchez")
public interface IControllerGps {

    @PostMapping( value = "/gpsdata")
    public CompletableFuture<ResponseEntity> gpsdata(@RequestBody String body);

    @GetMapping(value = "/gpsdata")
    public String getgpsdata();
}
