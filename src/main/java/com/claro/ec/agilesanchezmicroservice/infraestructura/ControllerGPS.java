package com.claro.ec.agilesanchezmicroservice.infraestructura;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import com.claro.ec.agilesanchezmicroservice.documentos.Track;
import com.claro.ec.agilesanchezmicroservice.repository.RepositoryTracking;
import com.google.gson.Gson;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

@Log4j2
@RestController
public class ControllerGPS implements IControllerGps {
	
	@Autowired
	RepositoryTracking repositoryTracking;

    @Override
    public CompletableFuture<ResponseEntity> gpsdata(String body) {
    	log.error(body);
    	Track track = new Gson().fromJson(body, Track.class);
    	track.setIdTracking(new BigDecimal(System.currentTimeMillis()));
        log.info(track.toString());
        return  CompletableFuture.completedFuture(repositoryTracking.save(track)).thenApply(ResponseEntity::ok);
    }

    @Override
    public String getgpsdata() {
        return "request";
    }
}
