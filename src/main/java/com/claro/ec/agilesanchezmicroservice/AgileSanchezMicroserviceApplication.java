package com.claro.ec.agilesanchezmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgileSanchezMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AgileSanchezMicroserviceApplication.class, args);
    }

}
