package com.claro.ec.agilesanchezmicroservice.documentos;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "Track")
public class Track {

	@Id
	BigDecimal idTracking;

	private static final double MIN_ALTITUDE_STEP = 8.0;
	private static final float MOVEMENT_SPEED_THRESHOLD = 0.5f; // The minimum speed (in m/s) to consider the user in
																// movement
	private static final float STANDARD_ACCURACY = 10.0f;
	private static final float SECURITY_COEFFICIENT = 1.7f;

	public static final int TRACK_TYPE_STEADY = 0;
	public static final int TRACK_TYPE_WALK = 1;
	public static final int TRACK_TYPE_MOUNTAIN = 2;
	public static final int TRACK_TYPE_RUN = 3;
	public static final int TRACK_TYPE_BICYCLE = 4;
	public static final int TRACK_TYPE_CAR = 5;
	public static final int TRACK_TYPE_FLIGHT = 6;

	// Variables
	private long id; // Saved in DB
	private String name = ""; // Saved in DB
	private String description = ""; // Saved in DB
	// The data related to the Start Point
	private double latitudeStart;
	private double longitudeStart;
	private double altitudeStart;
	private float accuracyStart = STANDARD_ACCURACY;// Saved in DB
	private float speedStart;
	private long timeStart;
	// The data related to the Last FIX
	// added to the track
	private long timeLastFix;
	// The data related to the End Point
	private double latitudeEnd;
	private double longitudeEnd;
	private double altitudeEnd;
	private float accuracyEnd = STANDARD_ACCURACY;// Saved in DB
	private float speedEnd;
	private long timeEnd;
	// The data related to the Point
	// stored as last Step for Distance calculation
	private double latitudeLastStepDistance;
	private double longitudeLastStepDistance;
	private float accuracyLastStepDistance = STANDARD_ACCURACY;// Saved in DB
	// The data related to the Point
	// stored as last Step for Altitude
	private double altitudeLastStepAltitude;
	private float accuracyLastStepAltitude = STANDARD_ACCURACY;// Saved in DB

	private double latitudeMin;
	private double longitudeMin;

	private double latitudeMax;
	private double longitudeMax;

	private long duration;
	private long durationMoving;

	private float distance;
	private float distanceInProgress;
	private long distanceLastAltitude;

	private double altitudeUp;
	private double altitudeDown;
	private double altitudeInProgress;

	private float speedMax;
	private float speedAverage;
	private float speedAverageMoving;

	private long numberOfLocations = 0; // Saved in DB
	private long numberOfPlacemarks = 0; // Saved in DB

	private int validMap = 1; // Saved in DB

	private boolean isSelected = false;

}
