package com.claro.ec.agilesanchezmicroservice.repository;

import java.math.BigDecimal;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.claro.ec.agilesanchezmicroservice.documentos.Track;

@Transactional
public interface RepositoryTracking extends MongoRepository<Track, BigDecimal> {

}
